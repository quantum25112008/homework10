import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Выберите номер месяца от 1 до 12:");
        int number = scanner.nextInt();

        switch(number){
            case 1,2,12:
                System.out.println("Вы выбрали время года зима");
                break;
            case 3,4,5:
                System.out.println("Вы выбрали время года весна");
                break;
            case 6,7,8:
                System.out.println("Вы выбрали время года лето");
                break;
            case 9,10,11:
                System.out.println("Вы выбрали время года осень");
                break;
            default:
                System.out.println("Вы выбрали не существующий месяц");
        }
    }
}